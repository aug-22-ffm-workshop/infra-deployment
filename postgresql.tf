resource "helm_release" "postgresql" {
  name       = "postgresql"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "postgresql"
  version    = "12.2.8"
  namespace  = "postgres"
  create_namespace = true

  values = [
    "${file("postgresql-values.yaml")}"
  ]
}